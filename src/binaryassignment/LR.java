package binaryassignment;

import java.util.Arrays;
import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.SimpleBounds;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.BOBYQAOptimizer;

/**
 * LR implements a Lagrangean relaxation of the assignment problem.
 *
 * As in the blog post and associated R notebook, we use three nonnegative
 * multiplier vectors: lambda for the upper limit on providers for a single
 * user, mu for the lower limit (1) on providers for a single user, and nu
 * for the upper limit on users assigned to a single provider.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class LR implements MultivariateFunction {
  private static final double FUZZ = 1e-6;  // used for comparisons to zero
  // Problem parameters.
  private final int nUsers;        // number of users
  private final int nServers;      // number of providers
  private final int uMax;          // maximum number of users for any provider
  private final int cMax;          // maximum number of providers for any user
  private final double[][] omega;  // service quality matrix
  // Duals.
  private double[] lambda;         // for upper bound on providers per user
  private double[] mu;             // for lower bound on providers per user
  private double[] nu;             // for upper bound on users per provider
  // Other.
  private long evals;              // number of function evaluations

  /**
   * Constructor.
   * @param problem the problem to solve
   */
  public LR(final Problem problem) {
    nUsers = problem.getNusers();
    nServers = problem.getNservers();
    uMax = problem.getUmax();
    cMax = problem.getCmax();
    omega = problem.getOmega();
  }

  /**
   * Computes reduced costs.
   * @return the reduced cost matrix
   */
  private double[][] reducedCosts() {
    double[][] rc = new double[nUsers][nServers];
    for (int u = 0; u < nUsers; u++) {
      for (int c = 0; c < nServers; c++) {
        rc[u][c] = omega[u][c] - lambda[u] + mu[u] - nu[c];
      }
    }
    return rc;
  }

  /**
   * Splits a vector of duals into component vectors lambda, mu and nu.
   * @param duals the vector to split
   */
  private void split(final double[] duals) {
    lambda = Arrays.copyOfRange(duals, 0, nUsers);
    mu = Arrays.copyOfRange(duals, nUsers, 2 * nUsers);
    nu = Arrays.copyOfRange(duals, 2 * nUsers, 2 * nUsers + nServers);
  }

  /**
   * Computes the Lagrangean function value for a given dual vector.
   * @param duals a vector containing (nonnegative) lambda, mu and nu
   * @return the value of the relaxed objective at that dual solution
   */
  public double value(final double[] duals) {
    double lr = 0;
    // Split the input vector into lambda, mu and nu.
    split(duals);
    // Sum the positive parts of the reduced costs.
    double[][] rc = reducedCosts();
    for (int u = 0; u < nUsers; u++) {
      for (int c = 0; c < nServers; c++) {
        lr += Math.max(0, rc[u][c]);
      }
    }
    // Add cMax times the sum of the lambdas.
    lr += cMax * Arrays.stream(lambda).sum();
    // Subtract the sum of the mus.
    lr -= Arrays.stream(mu).sum();
    // Add uMax times the sum of the nus.
    lr += uMax * Arrays.stream(nu).sum();
    // Count the function evaluation.
    evals += 1;
    return lr;
  }

  /**
   * Minimizes the Lagrangean function.
   * @param maxEvals the objective evaluation limit
   * @param ub an upper bound on the individual multipliers
   * @param initRadius initial trust region radius
   * @param terminalRadius trust region radius for convergence
   * @return the optimal multipliers and objective value
   * @throws TooManyEvaluationsException if the evaluation limit is exceeded
   */
  public PointValuePair optimize(final int maxEvals, final double ub,
                                 final double initRadius,
                                 final double terminalRadius)
         throws TooManyEvaluationsException {
    evals = 0;
    int nMult = 2 * nUsers + nServers;   // dimension of the multiplier vector
    // Start at the origin.
    double[] init = new double[nMult];   // all zeros by default
    // Set bounds.
    double[] lower = new double[nMult];  // all zeros by default
    double[] upper = new double[nMult];
    Arrays.fill(upper, ub);
    // Set up a solver.
    // Use two interpolation points more than the multiplier vector dimension.
    BOBYQAOptimizer solver =
      new BOBYQAOptimizer(nMult + 2, initRadius, terminalRadius);
    PointValuePair result =
      solver.optimize(new ObjectiveFunction(this),
                      GoalType.MINIMIZE,
                      new InitialGuess(init),
                      new SimpleBounds(lower, upper),
                      new MaxEval(maxEvals));
    return result;
  }

  /**
   * Converts the result of a Lagrangean search into a primal solution.
   * Note: The primal solution is not guaranteed to be feasible.
   * @param result the search result
   * @return the corresponding primal solution
   */
  public int[][] getSolution(final PointValuePair result) {
    int[][] d = new int[nUsers][nServers];
    // Extract the multipliers from the LR search result and split it.
    split(result.getPoint());
    // Compute the reduced costs.
    double[][] rc = reducedCosts();
    // Assign those pairings that have positive reduced costs.
    for (int u = 0; u < nUsers; u++) {
      for (int c = 0; c < nServers; c++) {
        if (rc[u][c] > FUZZ) {
          d[u][c] = 1;
        }
      }
    }
    return d;
  }

  /**
   * Gets the number of function evaluations performed during minimization of
   * the Lagrangean function.
   * @return the evaluation count
   */
  public long getEvalCount() {
    return evals;
  }
}
