package binaryassignment;

import ilog.concert.IloException;
import ilog.cplex.IloCplex.Status;
import java.util.Random;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.optim.PointValuePair;

/**
 * BinaryAssignment replicates an experiment involving Lagrangean relaxation,
 * originally done in R and discussed in a blog post
 * (https://orinanobworld.blogspot.com/2021/02/lagrangean-relaxation-for-assignment.html).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class BinaryAssignment {

  /**
   * Dummy constructor.
   */
  private BinaryAssignment() { }

  /**
   * Runs the experiment.
   *
   * Note: Parameter settings for the Lagrangean solver are largely from
   * trial and error.
   *
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Set the same problem dimensions used in the blog post.
    int nU = 10;    // number of users
    int nC = 5;     // number of providers
    int uMax = 4;   // maximum number of users any provider can serve
    int cMax = 3;   // maximum number of providers that can serve any one user
    long seed = 123;  // random number seed
    // Set up a random number generator.
    Random rng = new Random(seed);
    // Create a problem instance.
    Problem problem = new Problem(nU, nC, uMax, cMax, rng);
    // Solve an IP model to get the optimal solution.
    System.out.println("\nSolving the IP model:");
    try (IP ip = new IP(problem)) {
      Status status = ip.solve();
      System.out.println("Solver status = " + status);
      System.out.println("Final objective value = " + ip.getObjValue());
      int[][] sol = ip.getSolution();
      // Validate the solution.
      System.out.println(problem.validate(sol));
    } catch (IloException ex) {
      System.out.println("CPLEX exception:\n" + ex);
    }
    // Set up and solve a Lagrangean relaxation.
    System.out.println("\nAttempting Lagrangean relaxation ...");
    LR lagrange = new LR(problem);
    try {
      // Solve the Lagrangean problem.
      PointValuePair result = lagrange.optimize(10000, 2.0, 10.0, 1e-4);
      System.out.println(lagrange.getEvalCount()
                         + " evaluations were performed.");
      System.out.println("LR objective = " + result.getValue());
      // Recover the corresponding assignments and validate them.
      int[][] sol = lagrange.getSolution(result);
      System.out.println(problem.validate(sol));
    } catch (TooManyEvaluationsException ex) {
      System.out.println(ex);
    }
  }

}
