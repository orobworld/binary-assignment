package binaryassignment;

import java.util.Arrays;
import java.util.Random;

/**
 * Problem encapsulates a problem instance.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Problem {
  // Problem parameters.
  private final int nUsers;     // number of users
  private final int nServers;   // number of providers
  private final int uMax;       // maximum number of users for any provider
  private final int cMax;       // maximum number of providers for any user
  private final double[][] omega;  // service quality matrix

  /**
   * Constructor for a random instance.
   * @param nu number of users
   * @param ns number of providers
   * @param um max users per provider
   * @param cm max providers per user
   * @param rng the random number generator to use
   */
  public Problem(final int nu, final int ns, final int um, final int cm,
                 final Random rng) {
    nUsers = nu;
    nServers = ns;
    uMax = um;
    cMax = cm;
    // Generate a matrix of server qualities randomly.
    omega = new double[nUsers][];
    for (int u = 0; u < nUsers; u++) {
      omega[u] = rng.doubles(nServers).toArray();
    }
  }

  /**
   * Gets the number of users.
   * @return the number of users
   */
  public int getNusers() {
    return nUsers;
  }

  /**
   * Gets the number of providers.
   * @return the number of providers
   */
  public int getNservers() {
    return nServers;
  }

  /**
   * Gets the maximum number of users per provider.
   * @return the user maximum
   */
  public int getUmax() {
    return uMax;
  }

  /**
   * Gets the maximum number of providers for a user.
   * @return the provider maximum
   */
  public int getCmax() {
    return cMax;
  }

  /**
   * Gets the matrix of server quality values.
   * @return the server quality matrix
   */
  public double[][] getOmega() {
    double[][] c = new double[nUsers][];
    for (int u = 0; u < nUsers; u++) {
      c[u] = Arrays.copyOf(omega[u], nServers);
    }
    return c;
  }

  /**
   * Validates a candidate solution.
   * @param d the assignment matrix
   * @return a string report of any problems found
   */
  public String validate(final int[][] d) {
    StringBuilder sb = new StringBuilder();
    // Confirm that every user is assigned to between 1 and cMax providers.
    boolean ok = true;
    for (int u = 0; u < nUsers; u++) {
      int n = Arrays.stream(d[u]).sum();
      if (n < 1) {
        sb.append("User ").append(u).append(" is unassigned.\n");
        ok = false;
      } else if (n > cMax) {
        sb.append("User ").append(u).append(" is assigned to too many (")
          .append(n).append(") providers.\n");
      }
    }
    if (ok) {
      sb.append("All users have the correct number of assignments.\n");
    }
    // Confirm that no provider has too many users.
    ok = true;
    for (int c = 0; c < nServers; c++) {
      int count = 0;
      for (int u = 0; u < nUsers; u++) {
        if (d[u][c] >= 1) {
          count += 1;
        }
      }
      if (count > uMax) {
        sb.append("Provider ").append(c).append(" is assigned too many (")
          .append(count).append(") users.\n");
        ok = false;
      }
    }
    if (ok) {
      sb.append("All providers have an allowable number of users.\n");
    }
    // Compute the objective value.
    double obj = 0;
    for (int u = 0; u < nUsers; u++) {
      for (int c = 0; c < nServers; c++) {
        if (d[u][c] > 0) {
          obj += omega[u][c];
        }
      }
    }
    sb.append("Objective value = ").append(obj).append(".\n");
    return sb.toString();
  }
}
