package binaryassignment;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.Status;

/**
 * IP uses an integer program to find the optimal assignment. It requires a
 * licensed copy of the CPLEX optimizer.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class IP implements AutoCloseable {
  private static final double ROUND = 0.5;  // cutoff for rounding up
  private final IloCplex ip;       // IP model
  private final IloIntVar[][] d;   // binary assignment variables
  private final int nUsers;        // number of users
  private final int nServers;      // number of servers

  /**
   * Constructor.
   * @param problem the problem to solve
   * @throws IloException if the IP model cannot be built
   */
  public IP(final Problem problem) throws IloException {
    // Get the problem data.
    nUsers = problem.getNusers();
    nServers = problem.getNservers();
    double[][] omega = problem.getOmega();
    int uMax = problem.getUmax();
    int cMax = problem.getCmax();
    // Initialize a IP model.
    ip = new IloCplex();
    // Create the matrix of assignment variables and simultaneously build
    // the objective function.
    IloLinearNumExpr obj = ip.linearNumExpr();
    d = new IloIntVar[nUsers][nServers];
    for (int u = 0; u < nUsers; u++) {
      for (int c = 0; c < nServers; c++) {
        d[u][c] = ip.boolVar("d_" + u + "_" + "c");
        obj.addTerm(omega[u][c], d[u][c]);
      }
    }
    // Set the objective.
    ip.addMaximize(obj);
    // Assign each user to between 1 and cMax providers.
    for (int u = 0; u < nUsers; u++) {
      IloRange r = ip.le(ip.sum(d[u]), cMax);
      r.setLB(1.0);
      ip.add(r);
    }
    // Assign at most uMax users to any provider.
    for (int c = 0; c < nServers; c++) {
      IloLinearNumExpr expr = ip.linearNumExpr();
      for (int u = 0; u < nUsers; u++) {
        expr.addTerm(1.0, d[u][c]);
      }
      ip.addLe(expr, uMax);
    }
    // Suppress solver output.
    ip.setOut(null);
  }

  /**
   * Closes the problem instance (freeing the CPLEX objects).
   */
  @Override
  public void close() {
    ip.close();
  }

  /**
   * Solves the IP.
   * @return the final solver status
   * @throws IloException if CPLEX encounters an error
   */
  public Status solve() throws IloException {
    ip.solve();
    return ip.getStatus();
  }

  /**
   * Gets the final object value.
   * @return the objective value
   * @throws IloException if CPLEX balks
   */
  public double getObjValue() throws IloException {
    return ip.getObjValue();
  }

  /**
   * Gets the solution.
   * @return the assignment matrix
   * @throws IloException if there is no solution to be had
   */
  public int[][] getSolution() throws IloException {
    int[][] sol = new int[nUsers][nServers];
    for (int u = 0; u < nUsers; u++) {
      double[] x = ip.getValues(d[u]);
      for (int c = 0; c < nServers; c++) {
        sol[u][c] = (x[c] > ROUND) ? 1 : 0;
      }
    }
    return sol;
  }
}
