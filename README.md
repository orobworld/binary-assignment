# Binary Assignment via Lagrangean Relaxation #

### What is this repository for? ###

In a [blog post](https://orinanobworld.blogspot.com/2021/02/lagrangean-relaxation-for-assignment.html) I discussed how to solve an assignment problem (in which users can be assigned to multiple servers and servers can serve multiple users) using Lagrangean relaxation. The post included an [R notebook](http://rubin.msu.domains/blog/assignment.nb.html) in which I generated a random problem instance, found the optimal assignment using CPLEX, and then solved the Lagrangean problem using three different nonlinear algorithms (Nelder-Mead, Hooke-Jeeves and BFGS) from various R packages.

I wanted to try out a derivative-free nonlinear optimization algorithm in Java. After some searching of the web, I settled on [Powell's BOBYQA algorithm](http://www.optimization-online.org/DB_HTML/2010/05/2616.html), implemented in the [Apache Commons Mathematics Library](https://commons.apache.org/proper/commons-math/). The assignment problem seemed like a good test problem to use. So I am providing my source code here. It requires a working CPLEX installation and the Apache library but nothing else.

The code was developed using CPLEX 20.1 and Apache Commons Math 3.6.1.

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

